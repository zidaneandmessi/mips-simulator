#ifndef MIPS_LABEL_HPP
#define MIPS_LABEL_HPP

#include <map>

int labelNum, labelPos[1001], labelAt[1001]; // labelPos为label所指内存位置, labelAt为label对应的行号 
map <string, int> labelMap; 

int getLabelNum(string s, int at)
{
	int t = labelMap[s];
	if (t) return t;
	labelNum++;
	labelAt[labelNum] = at;
	return labelMap[s] = labelNum;
}

#endif