#ifndef MIPS_INSTRUCTION_HPP
#define MIPS_INSTRUCTION_HPP

#define LABEL 0
#define INSTRUCTION 1

int wrong, predict;
bool jump;

struct command
{
	bool type;
	string name, content;
	int at;
};

queue <command> q1;

command divide(string s)
{
	command t;
	bool b1 = 0, b2 = 0;
	for (int i = 0; i < s.size(); i++)
	{
		if (s[i] == '.') b1 = 1;
		else if (s[i] == ':') b2 = 1;
	}
	if (!b1 && b2)
	{
		t.type = LABEL;
		t.name = s.substr(0, s.size() - 1);
		while (t.name[0] == ' ' || t.name[0] == '\t') t.name.erase(0, 1);
		while (t.name[t.name.size() - 1] == ' ' || t.name[t.name.size() - 1] == '\t') t.name.erase(t.name.size() - 1, 1);
	}
	else
	{
		t.type = INSTRUCTION;
		int l = s.size(), st = 0, en = 0;
		for (int i = 0; i < l; i++)
		{
			if (s[i] == '.') st = i + 1;
			else if ((s[i] == ' ' || s[i] == '\t') && st)
			{
				en = i;
				break;
			}
		}
		if (!st)
		{
			for (int i = 0; i < l; i++)
				if (s[i] == ' ' || s[i] == '\t')
				{
					en = i;
					break;
				}
			if (!en) en = l;
		}
		t.name = s.substr(st, en - st);
		while (t.name[0] == ' ' || t.name[0] == '\t') t.name.erase(0, 1);
		while (t.name[t.name.size() - 1] == ' ' || t.name[t.name.size() - 1] == '\t') t.name.erase(t.name.size() - 1, 1);
		if (l - en - 1 > 0) t.content = s.substr(en + 1, l - en - 1);
	}
	return t;
}

struct instruction
{
	int name, Rsrc1, Rsrc2, address, Rdest, label, at, jump;
};

string names[] = {"add", "addu", "addi", "addiu", "sub", "subu", "mul", "mulu", "div", "divu", "xor", "xoru", "neg", "negu", "rem", "remu", "li", "seq", "sge", "sgt", "sle", "slt", "sne", "b", "beq", "bne", "bge", "ble", "bgt", "blt", "beqz", "bnez", "blez", "bgez", "bgtz", "bltz", "j", "jr", "jal", "jalr", "la", "lb", "lh", "lw", "sb", "sh", "sw", "move", "mfhi", "mflo", "nop", "syscall", "mul2", "mulu2", "div2", "divu2"};

#define ADD 0
#define ADDU 1
#define ADDI 2
#define ADDIU 3
#define SUB 4
#define SUBU 5
#define MUL 6
#define MULU 7
#define DIV 8
#define DIVU 9
#define XOR 10
#define XORU 11
#define NEG 12
#define NEGU 13
#define REM 14
#define REMU 15
#define LI 16
#define SEQ 17
#define SGE 18
#define SGT 19
#define SLE 20
#define SLT 21
#define SNE 22
#define B 23
#define BEQ 24
#define BNE 25
#define BGE 26
#define BLE 27
#define BGT 28
#define BLT 29
#define BEQZ 30
#define BNEZ 31
#define BLEZ 32
#define BGEZ 33
#define BGTZ 34
#define BLTZ 35
#define J 36
#define JR 37
#define JAL 38
#define JALR 39
#define LA 40
#define LB 41
#define LH 42
#define LW 43
#define SB 44
#define SH 45
#define SW 46
#define MOVE 47
#define MFHI 48
#define MFLO 49
#define NOP 50
#define SYSCALL 51
#define MUL2 52
#define MULU2 53
#define DIV2 54
#define DIVU2 55
 
int fileTot;
bool control_hazard;
queue <instruction> q2;

int str2Num(string s)
{
	int l =s.size(), d = 0;
	if (s[0] == '-') 
	{
		for (int i = 1; i < l; i++) d = d * 10 + s[i] - '0';
		return -d;
	}
	for (int i = 0; i < l; i++) d = d * 10 + s[i] - '0';
	return d;
}

bool getJump()
{
	if (wrong % 3 == 0) jump ^= 1;
	return jump;
}

bool parse(command &c)
{
	instruction t;
	t.at = c.at;	
	for (int i = 0; i <= 51; i++)
		if (c.name == names[i])
		{
			t.name = i;
			break;
		}
	vector <int> comma;
	for (int i = 0; i < c.content.size(); i++)
		if (c.content[i] == ',')
			comma.push_back(i);
	if (t.name == ADD) // add
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1; 
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == ADDU) // addu
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			string s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == ADDI) // addi
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3 = c.content.substr(l, r - l);
		int t2 = getRegNum(s2);
		if (use[t2]) return 0;
		t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
		if (use[t.Rdest]) return 0;
		use[t.Rdest] = 1;
	}
	else if (t.name == ADDIU) // addiu
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3 = c.content.substr(l, r - l);
		int t2 = getRegNum(s2);
		if (use[t2]) return 0;
		t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
		if (use[t.Rdest]) return 0;
		use[t.Rdest] = 1;
	}
	else if (t.name == SUB) // sub
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == SUBU) // subu
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == MUL && comma.size() == 2) // mul
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == MULU && comma.size() == 2) // mulu
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == MUL && comma.size() == 1) // mul2
	{
		t.name = MUL2;
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s2;
		if (c.content[l] == '$')
		{
			l++; s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1), t2 = getRegNum(s2);
			if (use[t1] || use[t2]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 = reg[t2];
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
		else
		{
			s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1); 
			if (use[t1]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 = str2Num(s2);
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
	}
	else if (t.name == MULU && comma.size() == 1) // mulu2
	{
		t.name = MULU2;
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s2;
		if (c.content[l] == '$')
		{
			l++; s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1), t2 = getRegNum(s2);
			if (use[t1] || use[t2]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 = reg[t2];
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
		else
		{
			s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1); 
			if (use[t1]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 = str2Num(s2);
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
	}
	else if (t.name == DIV && comma.size() == 2) // div
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == DIVU && comma.size() == 2) // divu
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == DIV && comma.size() == 1) // div2
	{
		t.name = DIV2;
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s2;
		if (c.content[l] == '$')
		{
			l++; s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1), t2 = getRegNum(s2);
			if (use[t1] || use[t2]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 = reg[t2];
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
		else
		{
			s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1);
			if (use[t1]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 = str2Num(s2);
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
	}
	else if (t.name == DIVU && comma.size() == 1) // divu2
	{
		t.name = DIVU2;
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s2;
		if (c.content[l] == '$')
		{
			l++; s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1), t2 = getRegNum(s2);
			if (use[t1] || use[t2]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 = reg[t2];
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
		else
		{
			s2 = c.content.substr(l, r - l);
			int t1 = getRegNum(s1);
			if (use[t1]) return 0;
			t.Rsrc1 = reg[t1], t.Rsrc2 =str2Num(s2);
			if (uslo || ushi) return 0;
			uslo = ushi = 1;
		}
	}
	else if (t.name == XOR) // xor
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == XORU) // xoru
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == NEG && comma.size() == 1 || t.name == MOVE) // neg, move
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		l++;
		string s2 = c.content.substr(l, r - l);
		int t2 = getRegNum(s2);
		if (use[t2]) return 0;
		t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2];
		if (use[t.Rdest]) return 0;
		use[t.Rdest] = 1;
	}
	else if (t.name == NEGU && comma.size() == 1) // negu
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s2 = c.content.substr(l, r - l);
		int t2 = getRegNum(s2);
		if (use[t2]) return 0;
		t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2];
		if (use[t.Rdest]) return 0;
		use[t.Rdest] = 1;
	}
	else if (t.name == REM) // rem
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == REMU) // remu
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == LI) // li
	{
		int l = 0, r = comma[0];
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		l++; 
		string s1 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s2 = c.content.substr(l, r - l);
		t.Rdest = getRegNum(s1), t.Rsrc2 = str2Num(s2);
		if (use[t.Rdest]) return 0;
		use[t.Rdest] = 1;
	}
	else if (t.name == SEQ || t.name == SGE || t.name == SGT || t.name == SLE || t.name == SLT || t.name == SNE) // seq, sge, sgt, sle, slt, sne
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] != '$') l++;
		l++; string s2 = c.content.substr(l, r - l);
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s3;
		if (c.content[l] == '$')
		{
			l++; s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2), t3 = getRegNum(s3);
			if (use[t2] || use[t3]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = reg[t3];
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
		else
		{
			s3 = c.content.substr(l, r - l);
			int t2 = getRegNum(s2);
			if (use[t2]) return 0;
			t.Rdest = getRegNum(s1), t.Rsrc1 = reg[t2], t.Rsrc2 = str2Num(s3);
			if (use[t.Rdest]) return 0;
			use[t.Rdest] = 1;
		}
	}
	else if (t.name == B || t.name == J || t.name == JAL) // b, j, jal
	{
		if (t.name == JAL)
		{
			if (use[31]) return 0;
			use[31] = 1;
		}
		t.label = getLabelNum(c.content, 0);
	}
	else if (t.name == BEQ || t.name == BNE || t.name == BGE || t.name ==  BLE || t.name == BGT || t.name == BLT) // beq, bne, bge, ble, bgt, blt
	{
		int l = 0, r = comma[0];
		l++; string s1 = c.content.substr(l, r - l);
		int regNum = getRegNum(s1);
		if (use[regNum]) return 0;
		t.Rsrc1 = reg[regNum];
		l = r + 1; r = comma[1];
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		string s2;
		if (c.content[l] == '$')
		{
			l++; s2 = c.content.substr(l, r - l);
			regNum = getRegNum(s2);
			if (use[regNum]) return 0;
		 	t.Rsrc2 = reg[regNum];
		}
		else
		{
			s2 = c.content.substr(l, r - l);
			t.Rsrc2 = str2Num(s2);
		}
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		t.label = getLabelNum(c.content.substr(l, r - l), 0);
	}
	else if (t.name == BEQZ || t.name == BNEZ || t.name == BGEZ || t.name == BLEZ || t.name == BGTZ || t.name == BLTZ) // beqz, bnez, bgez, blez, bgtz, bltz
	{
		int l = 0, r = comma[0];
		l++;
		int regNum = getRegNum(c.content.substr(l, r - l));
		if (use[regNum]) return 0;
		t.Rsrc1 = reg[regNum];
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		t.label = getLabelNum(c.content.substr(l, r - l), 0);
	}
	else if (t.name == JR || t.name == JALR) // jr, jalr
	{
		int regNum = getRegNum(c.content.substr(1, c.content.size() - 1));
		if (use[regNum]) return 0;
		if (t.name == JALR)
		{
			if (use[31]) return 0;
			use[31] = 1;
		}
		t.Rsrc1 = reg[regNum];
	}
	else if (t.name == LA || t.name == LB || t.name == LH || t.name == LW) // la, lb, lh, lw
	{
		if (t.name != LA && usmem) return 0;
		int l = 0, r = comma[0];
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		l++;
		t.Rdest = getRegNum(c.content.substr(l, r - l));
		if (use[t.Rdest]) return 0;
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		bool b = 0;
		int lbrac, rbrac;
		for (int i = l; i < c.content.size(); i++)
			if (c.content[i] == '(')
				b = 1, lbrac = i;
			else if (c.content[i] == ')')
		 		rbrac = i;
		if (b)
		{
			int shift, regNum = getRegNum(c.content.substr(lbrac + 2, rbrac - lbrac - 2));
			if (use[regNum]) return 0;
			if (l == lbrac) shift = 0;
			else shift = str2Num(c.content.substr(l, lbrac - l));
			t.address = reg[regNum] + shift;
		}
		else
			t.address = labelPos[getLabelNum(c.content.substr(l, r - l), 0)];
		use[t.Rdest] = 1;
	}
	else if (t.name == SB || t.name == SH || t.name == SW) // sb, sh, sw
	{
		if (usmem) return 0;
		int l = 0, r = comma[0];
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		l++;
		int regNum = getRegNum(c.content.substr(l, r - l));
		if (use[regNum]) return 0;
		t.Rsrc1 = reg[regNum];
		l = r + 1; r = c.content.size();
		while (l < r && c.content[l] == ' ' || c.content[l] == '\t') l++;
		bool b = 0;
		int lbrac, rbrac;
		for (int i = l; i < c.content.size(); i++)
			if (c.content[i] == '(')
				b = 1, lbrac = i;
			else if (c.content[i] == ')')
		 		rbrac = i;
		if (b)
		{
			int shift, regNum = getRegNum(c.content.substr(lbrac + 2, rbrac - lbrac - 2));
			if (use[regNum]) return 0;
			if (l == lbrac) shift = 0;
			else shift = str2Num(c.content.substr(l, lbrac - l));
			t.address = reg[regNum] + shift;
		}
		else
			t.address = labelPos[getLabelNum(c.content.substr(l, r - l), 0)];
		usmem = 1;
	}
	else if (t.name == MFHI) // mfhi
	{
		if (ushi) return 0;
		t.Rdest = getRegNum(c.content.substr(1, c.content.size() - 1));
		if (use[t.Rdest]) return 0;
		use[t.Rdest] = 1;
		t.Rsrc1 = hi;
	}
	else if (t.name == MFLO) // mflo
	{
		if (uslo) return 0;
		t.Rdest = getRegNum(c.content.substr(1, c.content.size() - 1));
		if (use[t.Rdest]) return 0;
		use[t.Rdest] = 1;
		t.Rsrc1 = lo;
	}
	else if (t.name == SYSCALL) // syscall
	{
		if (use[V0]) return 0;
		t.Rsrc1 = reg[V0];
		if (t.Rsrc1 == 1 && use[A0]) return 0;
		if (t.Rsrc1 == 4)
		{
			if (use[A0] || usmem) return 0;
		}
		if (t.Rsrc1 == 5) use[V0] = 1;
		if (t.Rsrc1 == 8)
		{
			if (use[A0] || use[A1] || usmem) return 0;
			usmem =1 ;
		}
		if (t.Rsrc1 == 9)
		{
			if (use[A0] || usmem) return 0;
			use[V0] = 1;
			usmem = 1;
		}
		if (t.Rsrc1 == 17 && use[A0]) return 0;
		if (t.Rsrc1 == 1 || t.Rsrc1 == 4 || t.Rsrc1 == 9 || t.Rsrc1 == 17 || t.Rsrc1 == 8) t.Rsrc2 = reg[A0];
		else t.Rsrc2 = 0;
	}
	if (t.name >= BEQ && t.name <= BLTZ)
	{
		if (getJump()) fileTot = labelAt[t.label], t.jump = 1;
		else t.jump = -1;
		control_hazard = 0;
		predict++;
	}
	q2.push(t);
	return 1;
}

#endif