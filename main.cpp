#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstddef>
#include <thread>
#include "register.hpp"
#include "label.hpp"
#include "memory.hpp"
#include "task.hpp"
#include "instruction.hpp"

using namespace std;

vector <string> file;

int curLabel;

void memoryInit()
{
	reg[SP] = SIZE - 1;
	for (int i = 0; i < file.size(); i++)
	{
		string s = file[i];
		command com = divide(s);
	 	if (com.type == LABEL)
		 	getLabelNum(com.name, i);
	 	if (s == ".data" && !fileTot)
			fileTot = i + 1;
	}
	while (file[fileTot] != ".text")
	{
		string s = file[fileTot++];
		command com = divide(s);
	 	if (com.type == LABEL)
	 	{
		 	curLabel = getLabelNum(com.name, 0);
	 		labelPos[curLabel] = memPos;
	 	}
	 	else if (com.name == "align")
	 	{
	 		int n = str2Num(com.content);
	 		int t = 1 << n;
	 		while (t < memPos) t += 1 << n;
	 		memPos = t;
	 	}
	 	else if (com.name == "ascii")
	 	{
	 		string s = com.content;
	 		for (int i = 1; i < s.size() - 1; i++)
	 		{
		 		if (s[i] == '\\' && s[i+1] == 'n')
		 		{
		 			pushInMemory(int('\n'), 1);
		 			i++;
		 		}
				else if (s[i] == '\\' && s[i+1] == 't')
		 		{
		 			pushInMemory(int('\t'), 1);
		 			i++;
		 		}
		 		else if (s[i] == '\\' && s[i+1] == '\\' && s[i - 1] != '\\')
		 		{
		 			pushInMemory(int('\\'), 1);
		 			i++;
		 		}
		 		else if (s[i] == '\\' && s[i+1] == '\"')
		 		{
		 			pushInMemory(int('\"'), 1);
		 			i++;
		 		}
	 			else pushInMemory(int(s[i]), 1);
		 	}
	 	}
	 	else if (com.name == "asciiz")
	 	{
	 		string s = com.content;
	 		for (int i = 1; i < s.size() - 1; i++)
	 		{
		 		if (s[i] == '\\' && s[i+1] == 'n')
		 		{
		 			pushInMemory(int('\n'), 1);
		 			i++;
		 		}
				else if (s[i] == '\\' && s[i+1] == 't')
		 		{
		 			pushInMemory(int('\t'), 1);
		 			i++;
		 		}
		 		else if (s[i] == '\\' && s[i+1] == '\\' && s[i - 1] != '\\')
		 		{
		 			pushInMemory(int('\\'), 1);
		 			i++;
		 		}
		 		else if (s[i] == '\\' && s[i+1] == '\"')
		 		{
		 			pushInMemory(int('\"'), 1);
		 			i++;
		 		}
	 			else pushInMemory(int(s[i]), 1);
		 	}
	 		pushInMemory('\0', 1);
	 	}
	 	else if (com.name == "byte")
	 	{
			vector <int> comma;
			for (int i = 0; i < com.content.size(); i++)
				if (com.content[i] == ',')
					comma.push_back(i);
			int pos = 0;
			while (pos < com.content.size() && com.content[pos] != '\'') pos++;
			pos++;
			pushInMemory(com.content[pos], 1);
			for (int i = 0; i < comma.size(); i++)
			{
				pos = comma[i] + 1;
				while (pos < com.content.size() && com.content[pos] != '\'') pos++;
				pos++;
				pushInMemory(com.content[pos], 1);
			}
	 	}
	 	else if (com.name == "half")
	 	{
	 		vector <int> comma;
			for (int i = 0; i < com.content.size(); i++)
				if (com.content[i] == ',')
					comma.push_back(i);
			int l = 0, r = -1;
			for (int i = 0; i < comma.size(); i++)
			{
				r = comma[i];
				while (l < r && (com.content[l] == ' ' || com.content[l] == '\t')) l++;
				pushInMemory(str2Num(com.content.substr(l, r - l)), 2);
				l = r + 1;
			}
			l = r + 1; r = com.content.size();
			while (l < r && (com.content[l] == ' ' || com.content[l] == '\t')) l++;
	 	}
	 	else if (com.name == "word")
	 	{
	 		vector <int> comma;
			for (int i = 0; i < com.content.size(); i++)
				if (com.content[i] == ',')
					comma.push_back(i);
			int l = 0, r = -1;
			for (int i = 0; i < comma.size(); i++)
			{
				r = comma[i];
				while (l < r && (com.content[l] == ' ' || com.content[l] == '\t')) l++;
				pushInMemory(str2Num(com.content.substr(l, r - l)), 4);
				l = r + 1;
			}
			l = r + 1; r = com.content.size();
			while (l < r && (com.content[l] == ' ' || com.content[l] == '\t')) l++;
			pushInMemory(str2Num(com.content.substr(l, r - l)), 4);
	 	}
	 	else if (com.name == "space")
	 	{
	 		int n = str2Num(com.content);
	 		memPos += n;
	 	}
	}
}

void fetch()
{
	command com;
	string s = file[fileTot];
 	com = divide(s);
 	while (com.type == LABEL)
 	{
	 	curLabel = getLabelNum(com.name, 0);
	 	if (!labelPos[curLabel]) labelPos[curLabel] = memPos;
	 	fileTot++;
	 	s = file[fileTot];
		com = divide(s);
 	}
 	if (com.name[0] == 'b' || com.name[0] == 'j') control_hazard = 1;
 	fileTot++;
	com.at = fileTot;
 	q1.push(com);
}

void execute(instruction &ins)
{
	vector <task> res;
	if (ins.name == ADD)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1 + ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == ADDU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (unsigned)ins.Rsrc1 + (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == ADDI)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1 + ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == ADDIU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (unsigned)ins.Rsrc1 + (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == SUB)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1 - ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == SUBU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (unsigned)ins.Rsrc1 - (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == MUL)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1 * ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == MULU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (unsigned)ins.Rsrc1 * (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == MUL2)
	{
		task a;
		a.type = SAVE;
		long long r = (long long)ins.Rsrc1 * ins.Rsrc2;
		a.dest = LO;
		a.data = (int)(r % (1LL << 32));
		res.push_back(a);
		a.dest = HI;
		a.data = (int)(r >> 32);
		res.push_back(a);
	}
	else if (ins.name == MULU2)
	{
		task a;
		a.type = SAVE;
		long long r = (long long)(unsigned)ins.Rsrc1 * (unsigned)ins.Rsrc2;
		a.dest = LO;
		a.data = (int)(r % (1LL << 32));
		res.push_back(a);
		a.dest = HI;
		a.data = (int)(r >> 32);
		res.push_back(a);
	}
	else if (ins.name == DIV)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1 / ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == DIVU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (unsigned)ins.Rsrc1 / (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == DIV2)
	{
		task a;
		a.type = SAVE;
		a.dest = LO;
		a.data = ins.Rsrc1 / ins.Rsrc2;
		res.push_back(a);
		a.dest = HI;
		a.data = ins.Rsrc1 % ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == DIVU2)
	{
		task a;
		a.type = SAVE;
		a.dest = LO;
		a.data = (unsigned)ins.Rsrc1 / (unsigned)ins.Rsrc2;
		res.push_back(a);
		a.dest = HI;
		a.data = (unsigned)ins.Rsrc1 % (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == XOR)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1 ^ ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == XORU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (unsigned)ins.Rsrc1 ^ (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == NEG)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = -ins.Rsrc1;
		res.push_back(a);
	}
	else if (ins.name == NEGU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = -(unsigned)ins.Rsrc1;
		res.push_back(a);
	}
	else if (ins.name == REM)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1 % ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == REMU)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (unsigned)ins.Rsrc1 % (unsigned)ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == LI)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc2;
		res.push_back(a);
	}
	else if (ins.name == SEQ)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (bool)(ins.Rsrc1 == ins.Rsrc2);
		res.push_back(a);
	}
	else if (ins.name == SGE)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (bool)(ins.Rsrc1 >= ins.Rsrc2);
		res.push_back(a);
	}
	else if (ins.name == SGT)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (bool)(ins.Rsrc1 > ins.Rsrc2);
		res.push_back(a);
	}
	else if (ins.name == SLE)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (bool)(ins.Rsrc1 <= ins.Rsrc2);
		res.push_back(a);
	}
	else if (ins.name == SLT)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (bool)(ins.Rsrc1 < ins.Rsrc2);
		res.push_back(a);
	}
	else if (ins.name == SNE)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = (bool)(ins.Rsrc1 != ins.Rsrc2);
		res.push_back(a);
	}
	else if (ins.name == B || ins.name == J)
	{
		fileTot = labelAt[ins.label];
		control_hazard = 0;
	}
	else if (ins.name == BEQ)
	{
		if (ins.Rsrc1 == ins.Rsrc2)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BNE)
	{
		if (ins.Rsrc1 != ins.Rsrc2)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BGE)
	{
		if (ins.Rsrc1 >= ins.Rsrc2)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BLE)
	{
		if (ins.Rsrc1 <= ins.Rsrc2)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BGT)
	{
		if (ins.Rsrc1 > ins.Rsrc2)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BLT)
	{
		if (ins.Rsrc1 < ins.Rsrc2)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BEQZ)
	{
		if (ins.Rsrc1 == 0)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BNEZ)
	{
		if (ins.Rsrc1 != 0)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BLEZ)
	{
		if (ins.Rsrc1 <= 0)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BGEZ)
	{
		if (ins.Rsrc1 >= 0)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BGTZ)
	{
		if (ins.Rsrc1 > 0)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == BLTZ)
	{
		if (ins.Rsrc1 < 0)
		{
			if (ins.jump == -1)
			{
				fileTot = labelAt[ins.label];
				while (!q1.empty()) q1.pop();
				while (!q2.empty()) q2.pop();
				for (int i = 0; i <= 31; i++) use[i] = 0;
				uslo = ushi = usmem = 0;
				wrong++; control_hazard = 0;
			}
		}
		else if (ins.jump == 1)
		{
			fileTot = ins.at;
			while (!q1.empty()) q1.pop();
			while (!q2.empty()) q2.pop();
			for (int i = 0; i <= 31; i++) use[i] = 0;
			uslo = ushi = usmem = 0;
			wrong++; control_hazard = 0;
		}
		ins.jump = 0;
	}
	else if (ins.name == JR)
	{
		fileTot = ins.Rsrc1;
		control_hazard = 0;
	}
	else if (ins.name == JAL)
	{
		task a;
		a.type = SAVE;
		a.dest = 31;
		a.data = fileTot;
		res.push_back(a);
		fileTot = labelAt[ins.label];
		control_hazard = 0;
	}
	else if (ins.name == JALR)
	{
		task a;
		a.type = SAVE;
		a.dest = 31;
		a.data = fileTot;
		res.push_back(a);
		fileTot = ins.Rsrc1;
		control_hazard = 0;
	}
	else if (ins.name == LA)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.address;
		res.push_back(a);
	}
	else if (ins.name == LB)
	{
		task a;
		a.type = LOAD;
		a.address = ins.address;
		a.tmp = 1;
		a.dest= ins.Rdest;
		res.push_back(a);
	}
	else if (ins.name == LH)
	{
		task a;
		a.type = LOAD;
		a.address = ins.address;
		a.tmp = 2;
		a.dest= ins.Rdest;
		res.push_back(a);
	}
	else if (ins.name == LW)
	{
		task a;
		a.type = LOAD;
		a.address = ins.address;
		a.tmp = 4;
		a.dest= ins.Rdest;
		res.push_back(a);
	}
	else if (ins.name == SB)
	{
		task a;
		a.type = STORE;
		a.address = ins.address;
		a.tmp = 1;
		a.data = ins.Rsrc1;
		res.push_back(a);
	}
	else if (ins.name == SH)
	{
		task a;
		a.type = STORE;
		a.address = ins.address;
		a.tmp = 2;
		a.data = ins.Rsrc1;
		res.push_back(a);
	}
	else if (ins.name == SW)
	{
		task a;
		a.type = STORE;
		a.address = ins.address;
		a.tmp = 4;
		a.data = ins.Rsrc1;
		res.push_back(a);
	}
	else if (ins.name == MOVE)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1;
		res.push_back(a);
	}
	else if (ins.name == MFHI || ins.name == MFLO)
	{
		task a;
		a.type = SAVE;
		a.dest = ins.Rdest;
		a.data = ins.Rsrc1;
		res.push_back(a);
	}
	else if (ins.name == NOP)
	{
		task a;
		a.type = NON;
		res.push_back(a);
	}
	else if (ins.name == SYSCALL)
	{
		task a;
		a.type = SYS;
		a.data = ins.Rsrc1;
		a.tmp = ins.Rsrc2;
		res.push_back(a);
	}
	q3.push(res);
}

void access(vector <task> &v)
{
	vector <task> res;
	int l = v.size();
	for (int i = 0; i < l; i++)
	{
		if (v[i].type == LOAD)
		{
			task a;
			a.type = SAVE;
			unsigned int t;
			if (v[i].tmp == 1) t = (unsigned)memory[v[i].address] % (1 << 8);
			else if (v[i].tmp == 2) t = (((unsigned)memory[v[i].address] % (1 << 8)) << 8) + ((unsigned)memory[v[i].address + 1] % (1 << 8));
			else if (v[i].tmp == 4) t = (((unsigned)memory[v[i].address] % (1 << 8)) << 24) + (((unsigned)memory[v[i].address + 1] % (1 << 8)) << 16)  + (((unsigned)memory[v[i].address + 2] % (1 << 8)) << 8) % (1 << 16) + ((unsigned)memory[v[i].address + 3] % (1 << 8));
			a.data = (int)t;
			a.dest = v[i].dest;
			res.push_back(a);
		}
		else if (v[i].type == STORE)
		{
			usmem = 0;
			unsigned int t = v[i].data;
			if (v[i].tmp == 1) memory[v[i].address] = (char)(t % (1 << 8));
			else if (v[i].tmp == 2)
			{
				memory[v[i].address] = (char)(t >> 8);
				memory[v[i].address + 1] = (char)(t % (1 << 8));
			}
			else if (v[i].tmp == 4)
			{
				memory[v[i].address] = (char)(t >> 24);
				memory[v[i].address + 1] = (char)((t >> 16) % (1 << 8));
				memory[v[i].address + 2] = (char)((t >> 8) % (1 << 8));
				memory[v[i].address + 3] = (char)(t % (1 << 8));
			}
		}
		else if (v[i].type == SYS)
		{
			if (v[i].data == 1)
		 		cout << v[i].tmp;
			else if (v[i].data == 4)
			{
				int t = v[i].tmp;
				for (int j = t; j < memPos && memory[j] != '\0'; j++)
					cout << memory[j];
			}
			else if (v[i].data == 5)
			{
				int t;
				cin >> t;
				task a;
				a.type = SAVE;
				a.data = t;
				a.dest = V0;
				res.push_back(a);
			}
			else if (v[i].data == 8)
			{
				string s;
				cin >> s;
				for (int j = 0; j < s.size(); j++)
					memory[v[i].tmp + j] = s[j];
				usmem = 0;
			}
			else if (v[i].data == 9)
			{
				task a;
				a.type = SAVE;
				a.data = memPos;
				a.dest = V0;
				res.push_back(a);
				memPos += v[i].tmp;
				usmem = 0;
			}
			else if (v[i].data == 10)
			{
				cerr<<(predict-wrong)*1.0/predict<<' ';
				exit(0);
			}
			else if (v[i].data == 17)
			{
				cerr<<(predict-wrong)*1.0/predict<<' ';
				exit(v[i].tmp);
			}
		}
		else if (v[i].type == SAVE || v[i].type == NON)
			res.push_back(v[i]);
	}
	q4.push(res);
}

void write(vector <task> &v)
{
	int l = v.size();
	for (int i = 0; i < l; i++)
		if (v[i].type == SAVE)
		{
			if (v[i].dest == LO) lo = v[i].data, uslo = 0;
			else if (v[i].dest == HI) hi = v[i].data, ushi = 0;
			else reg[v[i].dest] = v[i].data, use[v[i].dest] = 0;
		}
	v.clear();
}

void IF()
{
 	fetch();
}
void ID()
{
	if (parse(q1.front())) q1.pop();
}
void EX()
{
	instruction t = q2.front(); q2.pop();
	execute(t);
}
void MA()
{
	access(q3.front()); q3.pop();
}
void WB()
{
	write(q4.front()); q4.pop();
}

int main(int argc, char *argv[])
{
	ifstream fread(argv[1]);
	string s;
	while (getline(fread, s))
	{
		while (!s.empty() && s[0] == ' ' || s[0] == '\t') s.erase(0, 1);
		while (!s.empty() && s[s.size() - 1] == ' ' || s[s.size() - 1] == '\t') s.erase(s.size() - 1, 1);
		if (s.empty()) continue;
		if (s[0] == '#') continue;
		for (int i = 0; i < s.size(); i++)
			if (s[i] == '#')
			{
				s = s.substr(0, i);
				while (!s.empty() && s[s.size() - 1] == ' ' || s[s.size() - 1] == '\t') s.erase(s.size() - 1, 1);
				break;
			}
		file.push_back(s);
	}
	memoryInit();
	for (int i = 0; i < file.size(); i++)
		if (file[i] == "main:")
		{
			fileTot = i + 1;
			break;
		}
	int filesz = file.size();
	while (fileTot < filesz || !(q1.empty() && q2.empty() && q3.empty() && q4.empty()))
	{
		if (fileTot < filesz && !control_hazard) IF();
		bool f1 = q1.empty(), f2 = q2.empty(), f3 = q3.empty(), f4 = q4.empty();
		if (!f1 && !q1.empty()) ID();
		if (!f2 && !q2.empty()) EX();
		if (!f3 && !q3.empty()) MA();
 		if (!f4 && !q4.empty()) WB();
	}
	return 0;
}