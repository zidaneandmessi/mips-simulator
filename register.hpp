#ifndef MIPS_REGISTER_HPP
#define MIPS_REGISTER_HPP

#include <string>

#define ZERO 0
#define AT 1
#define V0 2
#define V1 3
#define A0 4
#define A1 5
#define A2 6
#define A3 7
#define T0 8
#define T1 9
#define T2 10
#define T3 11
#define T4 12
#define T5 13
#define T6 14
#define T7 15
#define S0 16
#define S1 17
#define S2 18
#define S3 19
#define S4 20
#define S5 21
#define S6 22
#define S7 23
#define T8 24
#define T9 25
#define K0 26
#define K1 27
#define GP 28
#define SP 29
#define S8 30
#define FP 30
#define RA 31
#define LO 32
#define HI 33

using namespace std;

int reg[32], lo, hi;
bool use[32], uslo, ushi, usmem;

int getRegNum(string s)
{
	int l = s.size();
	if (l == 1) return s[0] - '0';
	else if (l == 2 && s[0] >= '1' && s[0] <= '3') return (s[0] - '0') * 10 + s[1] - '0';
	else
	{
		if (s == "zero") return ZERO;
		if (s == "at") return AT;
		if (s == "v0") return V0;
		if (s == "v1") return V1;
		if (s == "a0") return A0;
		if (s == "a1") return A1;
		if (s == "a2") return A2;
		if (s == "a3") return A3;
		if (s == "t0") return T0;
		if (s == "t1") return T1;
		if (s == "t2") return T2;
		if (s == "t3") return T3;
		if (s == "t4") return T4;
		if (s == "t5") return T5;
		if (s == "t6") return T6;
		if (s == "t7") return T7;
		if (s == "s0") return S0;
		if (s == "s1") return S1;
		if (s == "s2") return S2;
		if (s == "s3") return S3;
		if (s == "s4") return S4;
		if (s == "s5") return S5;
		if (s == "s6") return S6;
		if (s == "s7") return S7;
		if (s == "t8") return T8;
		if (s == "t9") return T9;
		if (s == "k0") return K0;
		if (s == "k1") return K1;
		if (s == "gp") return GP;
		if (s == "sp") return SP;
		if (s == "s8") return S8;
		if (s == "fp") return FP;
		if (s == "ra") return RA;
	}
}

#endif
