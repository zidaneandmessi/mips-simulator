#ifndef MIPS_MEMORY_HPP
#define MIPS_MEMORY_HPP

#define SIZE 10000001

int memPos;
char memory[SIZE];

void pushInMemory(int data, int len)
{
	char s[10];
	for (int i = 0; i < len; i++)
	{
		s[i] = char(data % (1 << 8));
		data >>= 8;
	}
	for (int i = len - 1; i >= 0; i--)
		memory[memPos++] = s[i];
}
#endif