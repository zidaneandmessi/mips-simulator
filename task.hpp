#ifndef MIPS_TASK_HPP
#define MIPS_TASK_HPP
#include <queue>
#include <vector>

#define NON -1
#define LOAD 0
#define STORE 1
#define SAVE 2
#define SYS 3

struct task
{
	int type, address, data, dest, tmp;
};

queue < vector <task> > q3, q4;

#endif